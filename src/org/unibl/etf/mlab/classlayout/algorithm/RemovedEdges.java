package org.unibl.etf.mlab.classlayout.algorithm;

import java.util.ArrayList;

import org.unibl.etf.mlab.classlayout.diagram.Edge;

public class RemovedEdges
{
    private ArrayList<Edge> edges = new ArrayList<Edge>();

    public void addEdge(Edge edge)
    {
        this.edges.add(edge);
    }

    public void addEdges(ArrayList<Edge> edges)
    {
        this.edges.addAll(edges);
    }

    public ArrayList<Edge> getEdges()
    {
        return this.edges;
    }

    @Override
    public String toString()
    {
        String result = "";

        for (Edge edge : this.edges) {
            result = result + edge.toString() + "\n";
        }

        return result;
    }
}
