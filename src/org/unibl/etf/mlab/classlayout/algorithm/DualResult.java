package org.unibl.etf.mlab.classlayout.algorithm;

import java.util.HashMap;

import org.unibl.etf.mlab.classlayout.diagram.Diagram;
import org.unibl.etf.mlab.classlayout.diagram.Vertex;

public class DualResult
{
    private Diagram dual;
    private HashMap<String, HashMap<String, Vertex>> vertexToFaces;

    public DualResult(Diagram dual, HashMap<String, HashMap<String, Vertex>> vertexToFaces)
    {
        super();
        this.dual = dual;
        this.vertexToFaces = vertexToFaces;
    }

    public Diagram getDual()
    {
        return dual;
    }

    public HashMap<String, HashMap<String, Vertex>> getVertexToFaces()
    {
        return vertexToFaces;
    }
}
