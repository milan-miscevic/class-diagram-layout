package org.unibl.etf.mlab.classlayout.algorithm;

public class EdgeTriple
{
    protected String previous;
    protected String current;
    protected String next;

    public int direction;

    public EdgeTriple(String previous, String current, String next)
    {
        super();

        this.previous = previous;
        this.current = current;
        this.next = next;
    }

    public boolean isNext(EdgeTriple triple)
    {
        if (this.direction == triple.direction) {
            if (this.direction == 1) {
                if (
                    this.current.equals(triple.previous) && this.next.equals(triple.current)
                    ||
                    this.previous.equals(triple.current) && this.current.equals(triple.next)
                ) {
                    return true;
                }
            } else if (this.direction == -1) {
                if (
                    this.current.equals(triple.next) && this.previous.equals(triple.current)
                    ||
                    this.next.equals(triple.current) && this.current.equals(triple.previous)
                ) {
                    return true;
                }
            }
        } else {
            if (
                this.current.equals(triple.next) && this.next.equals(triple.current)
                ||
                this.current.equals(triple.previous) && this.previous.equals(triple.current)
            ) {
                return true;
            }
        }

        return false;
    }

    @Override
    public String toString()
    {
        return "\n" + this.previous + " " + this.current + " " + this.next + " " + this.direction + "\n";
    }
}
