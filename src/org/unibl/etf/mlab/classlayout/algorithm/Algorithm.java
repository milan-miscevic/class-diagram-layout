package org.unibl.etf.mlab.classlayout.algorithm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.unibl.etf.mlab.classlayout.diagram.Diagram;
import org.unibl.etf.mlab.classlayout.diagram.Edge;
import org.unibl.etf.mlab.classlayout.diagram.Vertex;

public class Algorithm
{
    final static int UP = 0;
    final static int RIGHT = 1;
    final static int DOWN = 2;
    final static int LEFT = 3;

    final static int DEFAULT_DISTANCE = 200;

    public static void layout(Diagram diagram)
    {
        long start = System.currentTimeMillis();

        int x = Algorithm.DEFAULT_DISTANCE;
        int y = Algorithm.DEFAULT_DISTANCE;

        ArrayList<Diagram> subdiagrams = Algorithm.divideDiagram(diagram);

        p(subdiagrams.size() + " diagrams");

        for (Diagram subdiagram : subdiagrams) {
            RemovedEdges removedReflexiveEdges = Algorithm.removeReflexiveEdges(subdiagram);
            RemovedEdges removedMultipleEdges = Algorithm.removeMultipleEdges(subdiagram);
            Collection<RemovedTree> removedTrees = Algorithm.removeTrees(subdiagram);
            ArrayList<ReplacedInheritance> replacedInheritances = Algorithm.replaceInheritances(subdiagram);

            Algorithm.planarize(subdiagram);

            Algorithm.restoreInheritances(subdiagram, replacedInheritances);
            Algorithm.restoreTrees(subdiagram, removedTrees);
            Algorithm.removeEmpty(subdiagram);
            // TODO sirine i visine
            Algorithm.restoreMultipleEdges(subdiagram, removedMultipleEdges);
            Algorithm.restoreReflexiveEdges(subdiagram, removedReflexiveEdges);

            int[] positions = Algorithm.moveToPositive(subdiagram, x, y);
            x = positions[Algorithm.RIGHT] + Algorithm.DEFAULT_DISTANCE;

            Algorithm.edge(subdiagram);
        }

        pt("layout(): " + (System.currentTimeMillis() - start));
    }

    private static ArrayList<Diagram> divideDiagram(Diagram diagram)
    {
        ArrayList<Diagram> subdiagrams = new ArrayList<Diagram>();
        HashMap<String, Vertex> visited = new HashMap<String, Vertex>();

        for (Vertex first : diagram.getVertices()) {
            if (visited.containsKey(first.getId())) {
                continue;
            }

            Diagram subdiagram = new Diagram();

            HashMap<String, Edge> edges = new HashMap<String, Edge>(); // hashmap to avoid edge duplication
            ArrayList<Vertex> currentVertices = new ArrayList<Vertex>();
            ArrayList<Vertex> nextVertices = new ArrayList<Vertex>();

            visited.put(first.getId(), first);
            currentVertices.add(first);

            do {
                for (Vertex vertex : currentVertices) {
                    subdiagram.addVertex(vertex);
                    for (Edge edge : vertex.getEdges()) {
                        edges.put(edge.getId(), edge);
                        Vertex outer = outer(edge, vertex);
                        if (!visited.containsKey(outer.getId())) {
                            visited.put(outer.getId(), outer);
                            nextVertices.add(outer);
                        }
                    }
                }

                currentVertices = nextVertices;
                nextVertices = new ArrayList<Vertex>();
            } while (currentVertices.size() > 0);

            subdiagram.addEdges(edges.values());
            subdiagrams.add(subdiagram);
        };

        return subdiagrams;
    }

    private static RemovedEdges removeReflexiveEdges(Diagram diagram)
    {
        RemovedEdges result = new RemovedEdges();

        for (Edge edge : diagram.getEdges()) {
            if (edge.getSource() == edge.getTarget()) {
                result.addEdge(edge);
            }
        }

        for (Edge edge : result.getEdges()) {
            diagram.removeEdge(edge);
        }

        return result;
    }

    private static void restoreReflexiveEdges(Diagram diagram, RemovedEdges edges)
    {
        for (Edge edge : edges.getEdges()) {
            Algorithm.drawReflexiveEdge(edge);
            diagram.addEdge(edge);
        }
    }

    private static RemovedEdges removeMultipleEdges(Diagram diagram)
    {
        RemovedEdges result = new RemovedEdges();

        for (Vertex vertex : diagram.getVertices()) {
            HashMap<String, Edge> unique = new HashMap<String, Edge>();
            ArrayList<Edge> toRemove = new ArrayList<Edge>();

            for (Edge edge : vertex.getEdges()) {
                Vertex outer = outer(edge, vertex);
                if (unique.containsKey(outer.getId())) {
                    if (edge.isGeneralization()) {
                        toRemove.add(unique.get(outer.getId()));
                        unique.put(outer.getId(), edge);
                    } else {
                        toRemove.add(edge);
                    }
                } else {
                    unique.put(outer.getId(), edge);
                }
            }

            result.addEdges(toRemove);

            for (Edge edge : toRemove) {
                diagram.removeEdge(edge);
            }
        }

        return result;
    }

    private static void restoreMultipleEdges(Diagram diagram, RemovedEdges removedEdges)
    {
        for (Edge removedEdge : removedEdges.getEdges()) {
            for (Edge edge : removedEdge.getSource().getEdges()) {
                if (
                    (removedEdge.getSource() == edge.getSource() && removedEdge.getTarget() == edge.getTarget())
                    ||
                    (removedEdge.getSource() == edge.getTarget() && removedEdge.getTarget() == edge.getSource())
                ) {
                    removedEdge.setBendPoints(edge.getBendPoints());
                    if (removedEdge.getSource() == edge.getSource()) {
                        removedEdge.setSourceAnchor(edge.getSourceAnchor());
                        removedEdge.setTargetAnchor(edge.getTargetAnchor());
                    } else {
                        removedEdge.setSourceAnchor(edge.getTargetAnchor());
                        removedEdge.setTargetAnchor(edge.getSourceAnchor());
                    }
                    diagram.addEdge(removedEdge);
                    break;
                }
            }
        }
    }

    private static Collection<RemovedTree> removeTrees(Diagram diagram)
    {
        HashMap<String, RemovedTree> removedTrees = new HashMap<String, RemovedTree>();

        ArrayList<Vertex> leafVertices;

        do {
            leafVertices = new ArrayList<Vertex>();

            for (Vertex vertex : diagram.getVertices()) {
                if (vertex.getEdgesCount() == 0) { // last one vertex, without edges
                    RemovedTree removedTree = new RemovedTree();
                    removedTree.base = vertex;
                    removedTree.tree.addVertex(removedTree.base);
                    removedTree.setConnection(null);
                    removedTrees.put(vertex.getId(), removedTree);

                    leafVertices.add(vertex);

                } else if (vertex.getEdgesCount() == 1) {
                    Edge edge = vertex.getFirstEdge();

                    if (edge.isGeneralization()) {
                        continue;
                    }

                    RemovedTree removeTree = new RemovedTree();
                    removeTree.base = vertex;
                    removeTree.tree.addVertex(removeTree.base);
                    removeTree.tree.addEdge(edge);
                    removeTree.setConnection(edge);
                    removedTrees.put(vertex.getId(), removeTree);

                    leafVertices.add(vertex);
                }
            }

            for (Vertex vertex : leafVertices) {
                diagram.removeVertex(vertex);
            }

        } while (leafVertices.size() > 0);

        RemovedTree leaf;

        do {
            leaf = null;

            out: for (RemovedTree removedTree : removedTrees.values()) {
                Edge connection = removedTree.getConnection();

                if (connection == null) {
                    continue;
                }

                Vertex outer = outer(connection, removedTree.base);

                for (RemovedTree parent : removedTrees.values()) {
                    if (parent.containsVertex(outer)) {
                        parent.tree.addVertices(removedTree.tree.getVertices());
                        parent.tree.addEdges(removedTree.tree.getEdges());
                        parent.tree.addEdge(removedTree.getConnection());
                        leaf = removedTree;
                        break out;
                    }
                }
            }

            if (leaf != null) {
                removedTrees.remove(leaf.base.getId());
            }

        } while (leaf != null);

        return removedTrees.values();
    }

    private static void restoreTrees(Diagram diagram, Collection<RemovedTree> removedTrees)
    {
        for (RemovedTree removedTree : removedTrees) {
            Edge connection = removedTree.getConnection();

            if (connection == null) { // whole diagram is tree
                Algorithm.drawTree(diagram, removedTree, Algorithm.RIGHT, 0, 0);
                return;
            }

            Vertex innerVertex = (diagram.containsVertex(connection.getSource()))
                ? connection.getSource()
                : connection.getTarget();

            int[] limits = Algorithm.calculateLimits(diagram);

            if (innerVertex.getX() == limits[Algorithm.RIGHT]) {
                Algorithm.drawTree(
                    diagram,
                    removedTree,
                    Algorithm.RIGHT,
                    innerVertex.getX(),
                    innerVertex.getY()
                );
            } else if (innerVertex.getX() == limits[Algorithm.LEFT]) {
                Algorithm.drawTree(
                    diagram,
                    removedTree,
                    Algorithm.LEFT,
                    innerVertex.getX(),
                    innerVertex.getY()
                );
            } else if (innerVertex.getY() == limits[Algorithm.UP]) {
                Algorithm.drawTree(diagram, removedTree, Algorithm.UP, innerVertex.getX(), innerVertex.getY());
            } else if (innerVertex.getY() == limits[Algorithm.DOWN]) {
                Algorithm.drawTree(diagram, removedTree, Algorithm.DOWN, innerVertex.getX(), innerVertex.getY());
            } else {
                int[] size = removedTree.getTreeSize();
                //p(size[0]);
                //p(size[1]);
                for (Vertex vertex : diagram.getVertices()) {
                    if (vertex.getY() > innerVertex.getY()) {
                        vertex.setY(vertex.getY() + size[1] * Algorithm.DEFAULT_DISTANCE);
                    }
                    // TODO width?
                }
                Algorithm.drawTree(diagram, removedTree, Algorithm.DOWN, innerVertex.getX(), innerVertex.getY());
            }

            Algorithm.drawDirectEdge(connection);
            diagram.addEdge(connection);
        }
    }

    private static void drawTree(Diagram diagram, Tree tree, int direction, int x, int y)
    {
        HashMap<String, Vertex> visited = new HashMap<String, Vertex>();
        visited.put(tree.base.getId(), tree.base);

        ArrayList<Vertex> currentVertices = new ArrayList<Vertex>();
        currentVertices.add(tree.base);

        ArrayList<Vertex> nextVertices = new ArrayList<Vertex>();

        int edgesCount = 0;

        boolean first = true;
        Vertex related = null;

        do {
            for (Vertex vertex : currentVertices) {
                for (Edge edge : vertex.getEdges()) {
                    Vertex outer = outer(edge, vertex);
                    if (visited.containsKey(outer.getId())) {
                        related = outer;
                    } else {
                        nextVertices.add(outer);
                        visited.put(outer.getId(), outer);
                        edgesCount++;
                    }
                }

                if (first) {
                    if (direction == Algorithm.UP) {
                        vertex.setX(x);
                        vertex.setY(y - Algorithm.DEFAULT_DISTANCE);
                    } else if (direction == Algorithm.RIGHT) {
                        vertex.setX(x + Algorithm.DEFAULT_DISTANCE);
                        vertex.setY(y);
                    } else if (direction == Algorithm.DOWN) {
                        vertex.setX(x);
                        vertex.setY(y + Algorithm.DEFAULT_DISTANCE);
                    } else {
                        vertex.setX(x - Algorithm.DEFAULT_DISTANCE);
                        vertex.setY(y);
                    }

                    first = false;
                } else {
                    int minDistance = Integer.MAX_VALUE;
                    int[] minPosition = new int[] {0, 0};
                    for (int[] position : Algorithm.around(diagram, related.getX(), related.getY())) {
                        int distance = (position[0] - x) * (position[0] - x) + (position[1] - y) * (position[1] - y);
                        if (distance < minDistance) {
                            minDistance = distance;
                            minPosition = position;
                        }
                    };
                    vertex.setX(minPosition[0]);
                    vertex.setY(minPosition[1]);
                }

                diagram.addVertex(vertex);
            }

            if (edgesCount == 0) {
                break;
            }

            currentVertices = nextVertices;
            nextVertices = new ArrayList<Vertex>();
            edgesCount = 0;
        } while (true);

        for (Edge e : tree.tree.getEdges()) {
            Algorithm.drawDirectEdge(e);
            diagram.addEdge(e);
        }
    }

    private static ArrayList<int[]> around(Diagram diagram, int x, int y)
    {
        ArrayList<int[]> result = new ArrayList<int[]>();

        int[][] variants = new int[][]{
            {x,                              y - Algorithm.DEFAULT_DISTANCE},
            {x + Algorithm.DEFAULT_DISTANCE, y - Algorithm.DEFAULT_DISTANCE},
            {x + Algorithm.DEFAULT_DISTANCE, y},
            {x + Algorithm.DEFAULT_DISTANCE, y + Algorithm.DEFAULT_DISTANCE},
            {x                             , y + Algorithm.DEFAULT_DISTANCE},
            {x - Algorithm.DEFAULT_DISTANCE, y + Algorithm.DEFAULT_DISTANCE},
            {x - Algorithm.DEFAULT_DISTANCE, y                             },
            {x - Algorithm.DEFAULT_DISTANCE, y - Algorithm.DEFAULT_DISTANCE}
        };

        for (int[] variant : variants) {
            if (diagram.getVertexAt(variant[0], variant[1]) == null) {
                result.add(variant);
            }
        }

        return result;
    }

    private static ArrayList<ReplacedInheritance> replaceInheritances(Diagram diagram)
    {
        ArrayList<ReplacedInheritance> result = new ArrayList<ReplacedInheritance>();

        int virtualVertexId = 1;
        int virtualEdgeId = 1;

        do {
            // find a edge of any inheritance hierarchy

            Vertex top = null;

            for (Edge edge : diagram.getEdges()) {
                if (edge.isGeneralization()) {
                    top = edge.getTarget();
                    break;
                }
            }

            if (top == null) {
                break;
            }

            // find the top of the inheritance hierarchy

            out: do {
                for (Edge edge : top.getEdges()) {
                    if (edge.isGeneralization() && edge.getSource() == top) {
                        top = edge.getTarget();
                        continue out;
                    }
                }
                break;
            } while (true);

            // replace

            ReplacedInheritance inheritance = new ReplacedInheritance();
            inheritance.base = top;

            Vertex virtualVertex = Algorithm.createVirtualVertex("inheritance" + virtualVertexId);
            diagram.addVertex(virtualVertex);
            inheritance.setVirtualVertex(virtualVertex);
            virtualVertexId++;

            HashMap<String, Vertex> internalVertices = Algorithm.collectVertices(top);
            ArrayList<Edge> internalEdges = new ArrayList<Edge>();

            for (Vertex vertex : internalVertices.values()) {
                for (Edge edge : vertex.getEdges()) {
                    Vertex outer = outer(edge, vertex);
                    if (internalVertices.containsKey(outer.getId())) {
                        internalEdges.add(edge);
                    } else {
                        Edge virtualEdge = Algorithm.createVirtualEdge("inheritance" + virtualEdgeId, virtualVertex, outer);
                        diagram.addEdge(virtualEdge);
                        virtualEdgeId++;
                        inheritance.addExternalEdge(edge);
                    }
                }

                inheritance.tree.addVertex(vertex);
                diagram.removeVertex(vertex);
            }

            inheritance.tree.addEdges(internalEdges);

            result.add(inheritance);
        } while (true);

        return result;
    }

    private static void restoreInheritances(Diagram diagram, ArrayList<ReplacedInheritance> inheritances)
    {
        for (ReplacedInheritance inheritance : inheritances) {
            int[] size = inheritance.getTreeSize();
            int toLeft, toRight;

            if (size[0] % 2 == 0) {
                toRight = size[0] / 2;
                toLeft = toRight - 1;
            } else {
                toLeft = toRight = (size[0] - 1) / 2;
            }

            diagram.removeVertex(inheritance.getVirtualVertex());
            // TODO check uguravanje
            for (Vertex vertex : diagram.getVertices()) {
                if (vertex.getX() < inheritance.getVirtualVertex().getX()) {
                    vertex.setX(vertex.getX() - toLeft * Algorithm.DEFAULT_DISTANCE);
                    p("levo:" + vertex.getId());
                } else if (vertex.getX() > inheritance.getVirtualVertex().getX()) {
                    p("desno:" + vertex.getId());
                    vertex.setX(vertex.getX() + toRight * Algorithm.DEFAULT_DISTANCE);
                } else {
                    if (vertex.getY() > inheritance.getVirtualVertex().getY()) {
                        vertex.setY(vertex.getY() + (size[1] - 1) * Algorithm.DEFAULT_DISTANCE);
                    }
                    p("doljeeeeee:" + vertex.getId());
                }
            }

            Algorithm.drawInheritance(diagram, inheritance, Algorithm.DOWN, inheritance.getVirtualVertex().getX(), inheritance.getVirtualVertex().getY(), true);

            for (Vertex v : inheritance.tree.getVertices()) {
                diagram.addVertex(v);
            }

            for (Edge edge : inheritance.getExternalEdges()) {
                diagram.addEdge(edge);
            }
        }
    }

    private static HashMap<String, Vertex> collectVertices(Vertex parent)
    {
        HashMap<String, Vertex> vertices = new HashMap<String, Vertex>();

        vertices.put(parent.getId(), parent);

        for (Edge edge : parent.getEdges()) {
            if (edge.isGeneralization() && edge.getTarget() == parent) {
                vertices.putAll(Algorithm.collectVertices(edge.getSource()));
            }
        }

        return vertices;
    }

    private static void drawInheritance(Diagram diagram, Tree tree, int direction, int x, int y, boolean onlyInteritance)
    {
        HashMap<String, Vertex> visited = new HashMap<String, Vertex>();
        visited.put(tree.base.getId(), tree.base);

        ArrayList<Vertex> currentVertices = new ArrayList<Vertex>();
        currentVertices.add(tree.base);

        ArrayList<Vertex> nextVertices = new ArrayList<Vertex>();

        int edgesCount = 0;

        do {
            int i = currentVertices.size() / 2 - currentVertices.size() + 1;

            for (Vertex v : currentVertices) {
                diagram.addVertex(v);
                if (direction == Algorithm.UP) {
                    v.setX(x + i * Algorithm.DEFAULT_DISTANCE);
                    v.setY(y);
                } else if (direction == Algorithm.RIGHT) {
                    v.setX(x);
                    v.setY(y + i * Algorithm.DEFAULT_DISTANCE);
                } else if (direction == Algorithm.DOWN) {
                    v.setX(x + i * Algorithm.DEFAULT_DISTANCE);
                    v.setY(y);
                } else {
                    v.setX(x);
                    v.setY(y + i * Algorithm.DEFAULT_DISTANCE);
                }
                i++;
                for (Edge e : v.getEdges()) {
                    if (onlyInteritance && !e.isGeneralization()) {
                        continue;
                    }
                    Vertex outer = (visited.containsKey(e.getSource().getId())) ? e.getTarget() : e.getSource();
                    if (!visited.containsKey(outer.getId())) {
                        nextVertices.add(outer);
                        visited.put(outer.getId(), outer);
                        edgesCount++;
                    }
                }
            }

            if (edgesCount == 0) {
                break;
            }

            currentVertices = nextVertices;
            nextVertices = new ArrayList<Vertex>();
            edgesCount = 0;

            if (direction == Algorithm.UP) {
                y -= Algorithm.DEFAULT_DISTANCE;
            } else if (direction == Algorithm.RIGHT) {
                x += Algorithm.DEFAULT_DISTANCE;
            } else if (direction == Algorithm.DOWN) {
                y += Algorithm.DEFAULT_DISTANCE;
            } else {
                x -= Algorithm.DEFAULT_DISTANCE;
            }

        } while (true);

        for (Edge e : tree.tree.getEdges()) {
            Algorithm.drawDirectEdge(e);
            diagram.addEdge(e);
        }
    }

    private static void planarize(Diagram diagram)
    {
        long start = System.currentTimeMillis();

        if (diagram.isEmpty()) {
            return;
        }

        // select start and end vertex

        Vertex startVertex = null;
        Vertex endVertex = null;

        ArrayList<Vertex> vertices = new ArrayList<Vertex>(diagram.getVertices());
        ArrayList<Vertex> initialVertices = new ArrayList<Vertex>();

        int tries;
        int maxDistance = 0;
        int sum = Integer.MAX_VALUE;

        if (vertices.size() < 3) {
            tries = vertices.size();
        } else if (vertices.size() < 10) {
            tries = 3;
        } else if (vertices.size() > 100) {
            tries = 10;
        } else {
            tries = (int) Math.floor(Math.sqrt(vertices.size()));
        }

        for (int i = 0; i < tries; i++) {
            int min = Integer.MAX_VALUE;
            int index = -1;

            for (int j = 0; j < vertices.size(); j++) {
                int newMin = vertices.get(j).getEdgesCount();
                if (newMin < min) {
                    min = newMin;
                    index = j;
                }
            }

            initialVertices.add(vertices.get(index));
            vertices.remove(index);
        }

        for (Vertex vertex : initialVertices) {
            DijkstraResult dijkstra = dijkstra(diagram, vertex);

            for (Map.Entry<String, Integer> entry : dijkstra.getDistances().entrySet()) {
                int oppositeDistance = entry.getValue();
                Vertex opositeVertex = diagram.getVertexById(entry.getKey());
                if (oppositeDistance > maxDistance) {
                    maxDistance = oppositeDistance;
                    startVertex = vertex;
                    endVertex = opositeVertex;
                    sum = startVertex.getEdgesCount() + endVertex.getEdgesCount();
                } else if (oppositeDistance == maxDistance) {
                    int newSum = vertex.getEdgesCount() + opositeVertex.getEdgesCount();
                    if (newSum < sum) {
                        startVertex = vertex;
                        endVertex = opositeVertex;
                        sum = startVertex.getEdgesCount() + endVertex.getEdgesCount();
                    }
                }
            }
        }

        // override for test

        // BigSugijama crossings
        //startVertex = diagram.getVertexById("_W-24UIK4EemvAf6eEEvq6A");
        //endVertex = diagram.getVertexById("_TCty4IK4EemvAf6eEEvq6A");

        // SimpleSugiyama dual
        //startVertex = diagram.getVertexById("_Q15BgMRMEemHlbYb4JJdLg");
        //endVertex = diagram.getVertexById("_0eiJUL3HEem754cAad92kQ");

        p(startVertex.getId() + " " + endVertex.getId());

        // arrange vertices by layers

        HashMap<String, Integer> visited = new HashMap<String, Integer>();
        visited.put(startVertex.getId(), 0);
        visited.put(endVertex.getId(), -1);

        ArrayList<HashMap<String, Vertex>> layers = new ArrayList<HashMap<String, Vertex>>();

        HashMap<String, Vertex> currentLayer = new HashMap<String, Vertex>();
        currentLayer.put(startVertex.getId(), startVertex);

        HashMap<String, Vertex> nextLayer = new HashMap<String, Vertex>();

        int layerId = 1;

        do {
            for (Vertex vertex : currentLayer.values()) {
                for (Edge edge : vertex.getEdges()) {
                    Vertex outer = outer(edge, vertex);
                    if (!visited.containsKey(outer.getId())) {
                        visited.put(outer.getId(), layerId);
                        nextLayer.put(outer.getId(), outer);
                    }
                }
            }

            layers.add(currentLayer);
            currentLayer = nextLayer;
            nextLayer = new HashMap<String, Vertex>();
            layerId++;

        } while (currentLayer.size() > 0);

        visited.put(endVertex.getId(), layerId - 1);
        nextLayer.put(endVertex.getId(), endVertex);
        layers.add(nextLayer);

        // rearrange layers with intra layer relations

        ArrayList<int[]> intraLayers = new ArrayList<int[]>();
        int move = 0;

        do {
            HashMap<String, Vertex> newLayer = null;
            int newLayerId = -1;

            for (int i = 0; i < layers.size(); i++) {
                currentLayer = layers.get(i);

                Vertex toCutOut = null;
                int maxRelations = 0;

                for (Vertex vertex : currentLayer.values()) {
                    int innerRelations = 0;

                    for (Edge edge : vertex.getEdges()) {
                        Vertex outer = outer(edge, vertex);
                        if (currentLayer.containsKey(outer.getId())) {
                            innerRelations++;
                        }
                    }

                    if (innerRelations > maxRelations) {
                        toCutOut = vertex;
                        maxRelations = innerRelations;
                    }
                }

                if (toCutOut != null) {
                    newLayer = new HashMap<String, Vertex>();
                    newLayer.put(toCutOut.getId(), toCutOut);

                    int beforeRelations = 0;
                    int afterRelations = 0;

                    for (Edge edge : toCutOut.getEdges()) {
                        Vertex outer = outer(edge, toCutOut);
                        int outerLayerId = visited.get(outer.getId());
                        if (outerLayerId < i) {
                            beforeRelations++;
                        }
                        if (outerLayerId > i) {
                            afterRelations++;
                        }
                    }

                    if (beforeRelations > afterRelations) {
                        newLayerId = i;
                        move = 1;
                    } else {
                        newLayerId = i + 1;
                        move = -1;
                    }

                    currentLayer.remove(toCutOut.getId());
                    break;
                }
            }

            if (newLayer != null) {
                layers.add(newLayerId, newLayer);
                intraLayers.add(new int[]{newLayerId, move});

                for (int i = 0; i < layers.size(); i++) {
                    for (Vertex vertex : layers.get(i).values()) {
                        visited.put(vertex.getId(), i);
                    }
                }
            } else {
                break;
            }
        } while (true);

        // insert dummy vertices and edges for edges that stretches two or more layers

        int virtualVertexId = 1;
        int virtualEdgeId = 1;

        HashMap<String, Vertex> virtualVertices = new HashMap<String, Vertex>();
        ArrayList<Edge> longEdges = new ArrayList<Edge>();
        ArrayList<Edge> virtualEdges = new ArrayList<Edge>();

        HashMap<String, ArrayList<String>> allMiddleVertices = new HashMap<String, ArrayList<String>>();
        ArrayList<String> middleVertices = new ArrayList<String>();

        for (Edge edge : diagram.getEdges()) {
            Vertex leftVertex = edge.getSource();
            Vertex rightVertex = edge.getTarget();
            int leftLayerId = visited.get(leftVertex.getId());
            int rightLayerId = visited.get(rightVertex.getId());

            if (leftLayerId > rightLayerId) {
                int tempLayerId = leftLayerId;
                leftLayerId = rightLayerId;
                rightLayerId = tempLayerId;

                Vertex tempVertex = leftVertex;
                leftVertex = rightVertex;
                rightVertex = tempVertex;
            }

            if (rightLayerId - leftLayerId >= 2) {
                middleVertices = new ArrayList<String>();
                allMiddleVertices.put(edge.getId(), middleVertices);

                Vertex virtualVertex = null;
                Edge virtualEdge;

                for (int i = leftLayerId + 1; i < rightLayerId; i++) {
                    virtualVertex = Algorithm.createVirtualVertex("sugiyama" + virtualVertexId);
                    diagram.addVertex(virtualVertex);
                    visited.put(virtualVertex.getId(), i);
                    layers.get(i).put(virtualVertex.getId(), virtualVertex);
                    virtualVertices.put(virtualVertex.getId(), virtualVertex);
                    middleVertices.add(virtualVertex.getId());
                    virtualVertexId++;

                    virtualEdge = Algorithm.createVirtualEdge("sugiyama" + virtualEdgeId, leftVertex, virtualVertex);
                    virtualEdges.add(virtualEdge);
                    virtualEdgeId++;

                    leftVertex = virtualVertex;
                }

                virtualEdge = Algorithm.createVirtualEdge("sugiyama" + virtualEdgeId, virtualVertex, rightVertex);
                virtualEdges.add(virtualEdge);
                virtualEdgeId++;

                longEdges.add(edge);
            }
        }

        diagram.removeEdges(longEdges);
        diagram.addEdges(virtualEdges);

        // assign coordinates

        int x = 0;
        int y = 0;

        for (HashMap<String, Vertex> layer : layers) {
            for (Vertex vertex : layer.values()) {
                vertex.setX(x);
                vertex.setY(y);
                y += Algorithm.DEFAULT_DISTANCE;
            }

            x += Algorithm.DEFAULT_DISTANCE;
            y = 0;
        }

        // crossing minimization

        long crossing = System.currentTimeMillis();

        for (int i = 0; i < 21; i++) { // TODO how many
            Algorithm.minimizeCrossings(layers, (i % 2 == 1));
        }

        pt("minimizeCrossings(): " + (System.currentTimeMillis() - crossing));

        // remove dummy vertices and edges, restore edges

        diagram.removeVertices(virtualVertices.values());
        diagram.addEdges(longEdges);

        // remove intra layers

        for (int i = intraLayers.size() - 1; i >= 0; i--) {
            int[] intraLayer = intraLayers.get(i);
            layerId = intraLayer[0];
            move = intraLayer[1];

            for (Vertex intraVertex : layers.get(layerId).values()) {
                if (!intraVertex.isVirtual()) {
                    int oldX = intraVertex.getX();
                    x = oldX + move * Algorithm.DEFAULT_DISTANCE;

                    if (diagram.getVertexAt(x, intraVertex.getY()) == null) {
                        intraVertex.setX(x);

                        for (Vertex vertex : diagram.getVertices()) {
                            x = vertex.getX();
                            if (move == 1 && x > oldX) {
                                x -= Algorithm.DEFAULT_DISTANCE;
                                vertex.setX(x);
                            } else if (move == -1 && x >= oldX) {
                                x -= Algorithm.DEFAULT_DISTANCE;
                                vertex.setX(x);
                            }

                        }
                    }
                }
            }

            layers.remove(layerId);
        }

        if (layers.size() > 5) {
            Algorithm.moveToAdjancedLayer(diagram, startVertex, Algorithm.DEFAULT_DISTANCE);
            Algorithm.moveToAdjancedLayer(diagram, endVertex, -Algorithm.DEFAULT_DISTANCE);
        }

        for (Edge edge : diagram.getEdges()) {
            Algorithm.drawDirectEdge(edge);
        }

        // TODO sta sa ovim?
//        for (Edge edge : longEdges) {
//            middleVertices = allMiddleVertices.get(edge.getId());
//
//            if (edge.getSource().getX() > edge.getTarget().getX()) {
//                Collections.reverse(middleVertices);
//            }
//
//            int[][] initial = edge.getBendPoints();
//            int[][] bendPoints = new int[middleVertices.size() + 2][2];
//
//            int i = 1;
//
//            for (String middleVertex : middleVertices) {
//                Vertex v = virtualVertices.get(middleVertex);
//                bendPoints[i][0] = v.getX() + v.getWidth() / 2;
//                bendPoints[i][1] = v.getY() + v.getHeight() / 2;
//                i++;
//            }
//
//            bendPoints[0][0] = initial[0][0];
//            bendPoints[0][1] = initial[0][1];
//            bendPoints[i][0] = initial[1][0];
//            bendPoints[i][1] = initial[1][1];
//
//            edge.setBendPoints(bendPoints);
//        }

        //

//        ArrayList<Edge> removed = new ArrayList<Edge>();
//
//        do {
//            HashMap<String, Integer> crossings = Algorithm.countCrossings(diagram);
//
//            if (crossings.size() == 0) {
//                break;
//            }
//
//            int max = Integer.MIN_VALUE;
//            String maxId = "";
//
//            for (Map.Entry<String, Integer> entry : crossings.entrySet()) {
//                int value = entry.getValue();
//                if (value > max) {
//                    value = max;
//                    maxId = entry.getKey();
//                }
//            }
//
//            Edge toRemove = diagram.getEdgeById(maxId);
//            removed.add(toRemove);
//            diagram.removeEdge(toRemove);
//        } while (true);
//
//        //p(removed);
//
//        DualResult dual = Algorithm.createDualDiagram(diagram);
//        HashMap<String, HashMap<String, Vertex>> v2f = dual.getVertexToFaces();
//
//        for (Edge edge : removed) {
//            HashMap<String, Vertex> sourceFaces = v2f.get(edge.getSource().getId());
//            HashMap<String, Vertex> targetFaces = v2f.get(edge.getTarget().getId());
//
//            int minDistance = Integer.MAX_VALUE;
//
//            for (Vertex v1 : sourceFaces.values()) {
//                DijkstraResult dr = dijkstra(dual.getDual(), v1);
//                HashMap<String, Integer> distances = dr.getDistances();
//                for (Vertex v2 : targetFaces.values()) {
//                    int distance = distances.get(v2.getId());
//                    if (distance < minDistance) {
//                        minDistance = distance;
//                        // TODO path
//                        // TODO insert dummy
//                    }
//                }
//            }
//
//            diagram.addEdge(edge);
//            Algorithm.drawDirectEdge(edge);
//
//            int[][] initial = edge.getBendPoints();
//            int[][] bendPoints = new int[3][2];
//
//            bendPoints[0][0] = initial[0][0];
//            bendPoints[0][1] = initial[0][1];
//            bendPoints[1][0] = -400;
//            bendPoints[1][1] = -400;
//            bendPoints[2][0] = initial[1][0];
//            bendPoints[2][1] = initial[1][1];
//
//            edge.setBendPoints(bendPoints);
//
//            //p(minDistance);
//        }

        //diagram.addEdges(removed);

        pt("planarize(): " + (System.currentTimeMillis() - start));
    }

    private static DijkstraResult dijkstra(Diagram diagram, Vertex start)
    {
        HashMap<String, Vertex> vertices = new HashMap<String, Vertex>();
        HashMap<String, Integer> distances = new HashMap<String, Integer>();
        HashMap<String, Vertex> previous = new HashMap<String, Vertex>();
        Vertex currentVertex = start;

        for (Vertex vertex : diagram.getVertices()) {
            vertices.put(vertex.getId(), vertex);
            distances.put(vertex.getId(), Integer.MAX_VALUE);
            previous.put(vertex.getId(), null);
        }

        distances.put(start.getId(), 0);

        while (vertices.size() > 0) {
            int minDistance = Integer.MAX_VALUE;

            for (Vertex vertex : vertices.values()) {
                int distance = distances.get(vertex.getId());
                if (distance < minDistance) {
                    minDistance = distance;
                    currentVertex = vertex;
                }
            }

            vertices.remove(currentVertex.getId());

            for (Edge edge : currentVertex.getEdges()) {
                Vertex outer = outer(edge, currentVertex);
                int newDistance = minDistance + 1;
                if (newDistance < distances.get(outer.getId())) {
                    distances.put(outer.getId(), newDistance);
                    previous.put(outer.getId(), currentVertex);
                }
            }
        }

        return new DijkstraResult(distances, previous);
    }

    private static void minimizeCrossings(ArrayList<HashMap<String, Vertex>> layers, boolean reverse)
    {
        int layerCount = layers.size();

        if (layerCount <= 2) {
            return;
        }

        for (int i = 0; i < layerCount - 2; i++) {
            HashMap<String, Vertex> currentLayer;
            HashMap<String, Vertex> nextLayer;

            if (reverse) {
                currentLayer = layers.get(layerCount - i - 1);
                nextLayer = layers.get(layerCount - i - 2);
            } else {
                currentLayer = layers.get(i);
                nextLayer = layers.get(i + 1);
            }

            HashMap<String, Integer> averages = new HashMap<String, Integer>();

            for (Vertex vertex : nextLayer.values()) {
                int sum = 0;
                int count = 0;

                for (Edge edge : vertex.getEdges()) {
                    Vertex outer = outer(edge, vertex);

                    if (currentLayer.containsKey(outer.getId())) {
                        sum += outer.getY();
                        count++;
                    }
                }

                if (count == 0) {
                    averages.put(vertex.getId(), 0); // TODO or some another number?
                } else {
                    averages.put(vertex.getId(), sum / count);
                }
            }

            int y = 0;

            do {
                String vertexId = null;
                int minimum = Integer.MAX_VALUE;

                for (Map.Entry<String, Integer> entry : averages.entrySet()) {
                    if (entry.getValue() < minimum) {
                        vertexId = entry.getKey();
                        minimum = entry.getValue();
                    }
                }

                averages.remove(vertexId);

                Vertex vertex = nextLayer.get(vertexId);
                vertex.setY(y);

                y += Algorithm.DEFAULT_DISTANCE;
            } while (averages.size() > 0);
        }
    }

    private static HashMap<String, Integer> countCrossingsLayer(ArrayList<HashMap<String, Vertex>> layers)
    {
        HashMap<String, Integer> crossings = new HashMap<String, Integer>();

        for (int i = 0; i < layers.size() - 1; i++) {
            HashMap<String, Vertex> currentLayer = layers.get(i);
            HashMap<String, Vertex> nextLayer = layers.get(i + 1);

            ArrayList<Edge> inter = new ArrayList<Edge>();

            for (Vertex vertex : currentLayer.values()) {
                for (Edge edge : vertex.getEdges()) {
                    Vertex outer = outer(edge, vertex);
                    if (nextLayer.containsKey(outer.getId())) {
                        inter.add(edge);
                    }
                }
            }

            for (Edge edge1 : inter) {
                for (Edge edge2 : inter) {

                    Vertex l1 = (edge1.getSource().getX() > edge1.getTarget().getX()) ? edge1.getTarget() : edge1.getSource();
                    Vertex l2 = (edge2.getSource().getX() > edge2.getTarget().getX()) ? edge2.getTarget() : edge2.getSource();

                    Vertex r1 = (edge1.getSource().getX() > edge1.getTarget().getX()) ? edge1.getSource() : edge1.getTarget();
                    Vertex r2 = (edge2.getSource().getX() > edge2.getTarget().getX()) ? edge2.getSource() : edge2.getTarget();

                    int toExchange = (r1.getY() - r2.getY()) * (l1.getY() - l2.getY());

                    if (toExchange < 0) {
                        int c = crossings.getOrDefault(edge1.getId(), 0);
                        c++;
                        crossings.put(edge1.getId(), c);
                        c = crossings.getOrDefault(edge2.getId(), 0);
                        c++;
                        crossings.put(edge2.getId(), c);
                    }
                }
            }
        }

        return crossings;
    }

    private static HashMap<String, Integer> countCrossings(Diagram diagram)
    {
        HashMap<String, Integer> crossings = new HashMap<String, Integer>();

//        if (diagram.getEdgeById("_6Oyg8FrQEem2RbYFVWZdng") != null) {
//            crossings.put("_6Oyg8FrQEem2RbYFVWZdng", 1); // TODO
//        }
//
//        for (Edge edge1 : diagram.getEdges()) {
//            //p(edge1);
//            Vertex source1 = edge1.getSource();
//            Vertex target1 = edge1.getTarget();
//            double a = (target1.getY() - source1.getY()) / (target1.getX() - source1.getX());
//            double b = target1.getY() - source1.getY();
//
//            for (Edge edge2 : diagram.getEdges()) {
//                Vertex source2 = edge2.getSource();
//                Vertex target2 = edge2.getTarget();
//                double c = (target2.getY() - source2.getY()) / (target2.getX() - source2.getX());
//                double d = target2.getY() - source2.getY();
//
//                double x = (d-b)/(a-c);
//
//                if (source1.getX() < x && x < target1.getX()) {
//                    //p(edge1);
//                    //p(edge2);
//                }
//
//                if (target1.getX() < x && x < source1.getX()) {
//                    //p(edge1);
//                    //p(edge2);
//                }
//
//            }
//        }

//        for (int i = 0; i < layers.size() - 1; i++) {
//            HashMap<String, Vertex> currentLayer = layers.get(i);
//            HashMap<String, Vertex> nextLayer = layers.get(i + 1);
//
//            ArrayList<Edge> inter = new ArrayList<Edge>();
//
//            for (Vertex vertex : currentLayer.values()) {
//                for (Edge edge : vertex.getEdges()) {
//                    Vertex outer = outer(edge, vertex);
//                    if (nextLayer.containsKey(outer.getId())) {
//                        inter.add(edge);
//                    }
//                }
//            }
//
//            for (Edge edge1 : inter) {
//                for (Edge edge2 : inter) {
//
//                    Vertex l1 = (edge1.getSource().getX() > edge1.getTarget().getX()) ? edge1.getTarget() : edge1.getSource();
//                    Vertex l2 = (edge2.getSource().getX() > edge2.getTarget().getX()) ? edge2.getTarget() : edge2.getSource();
//
//                    Vertex r1 = (edge1.getSource().getX() > edge1.getTarget().getX()) ? edge1.getSource() : edge1.getTarget();
//                    Vertex r2 = (edge2.getSource().getX() > edge2.getTarget().getX()) ? edge2.getSource() : edge2.getTarget();
//
//                    int toExchange = (r1.getY() - r2.getY()) * (l1.getY() - l2.getY());
//
//                    if (toExchange < 0) {
//                        int c = crossings.getOrDefault(edge1.getId(), 0);
//                        c++;
//                        crossings.put(edge1.getId(), c);
//                        c = crossings.getOrDefault(edge2.getId(), 0);
//                        c++;
//                        crossings.put(edge2.getId(), c);
//                    }
//                }
//            }
//        }

        return crossings;
    }

    private static DualResult createDualDiagram(Diagram diagram)
    {
        long start = System.currentTimeMillis();

        // calculate adjanced edges for each edge

        HashMap<String, HashMap<String, AdjancedEdges>> allOrders = new HashMap<String, HashMap<String, AdjancedEdges>>();
        HashMap<String, String> pairs = new HashMap<String, String>();

        for (Vertex vertex : diagram.getVertices()) {
            String[] angleIds = new String[vertex.getEdgesCount()];
            Double[] angleValues = new Double[vertex.getEdgesCount()];

            int i = 0;

            for (Edge edge : vertex.getEdges()) {
                int xdiff, ydiff, outerX, outerY;
                Vertex outer = outer(edge, vertex);

                if (edge.getBendPointsCount() > 2) {
                    int[][] bendPoints = edge.getBendPoints();
                    if (edge.getSource() == vertex) {
                        outerX = bendPoints[1][0];
                        outerY = bendPoints[1][1];
                    } else {
                        outerX = bendPoints[edge.getBendPointsCount() - 2][0];
                        outerY = bendPoints[edge.getBendPointsCount() - 2][1];
                    }
                } else {
                    outerX = outer.getX();
                    outerY = outer.getY();
                }

                xdiff = outerX - vertex.getX();
                ydiff = outerY - vertex.getY();

                double hypotenuse = Math.sqrt(xdiff * xdiff + ydiff * ydiff);
                double angle = Math.asin(xdiff/hypotenuse);

                if (ydiff > 0) {
                    angle = Math.PI - angle;
                } else {
                    if (xdiff < 0) {
                        angle = 2 * Math.PI + angle;
                    }
                }

                angleIds[i] = edge.getId();
                angleValues[i] = angle;
                i++;
            }

            String[] orderedAngles = new String[vertex.getEdgesCount()];

            i = 0;

            do {
                double minimal = Double.MAX_VALUE;
                int id = 0;

                for (int j = 0; j < angleValues.length; j++) {
                    if (angleValues[j] != null && angleValues[j] < minimal) {
                        minimal = angleValues[j];
                        id = j;
                    }
                }

                orderedAngles[i] = angleIds[id];
                angleValues[id] = null;
                i++;

            } while (i < vertex.getEdgesCount());

            HashMap<String, AdjancedEdges> ordered = new HashMap<String, AdjancedEdges>();
            allOrders.put(vertex.getId(), ordered);

            for (i = 0; i < orderedAngles.length; i++) {
                String previous = orderedAngles[(i + orderedAngles.length - 1) % orderedAngles.length];
                String next = orderedAngles[(i + 1) % orderedAngles.length];

                AdjancedEdges adjancedEdges = new AdjancedEdges(previous, next);
                ordered.put(orderedAngles[i], adjancedEdges);

                pairs.put(vertex.getId() + " " + orderedAngles[i], next);
            }
        }

        //p(pairs);

        // create faces and fill dual diagram with vertices

        Diagram dual = new Diagram();
        int id = 1;

        HashMap<String, Vertex[]> edgeToFace = new HashMap<String, Vertex[]>();

        for (Edge edge : diagram.getEdges()) {
            edgeToFace.put(edge.getId(), new Vertex[2]);
        }

        out: while (pairs.size() > 0) {
            for (Vertex vertex : diagram.getVertices()) {
                for (Edge edge : vertex.getEdges()) {
                    if (!pairs.containsKey(vertex.getId() + " " + edge.getId())) {
                        continue;
                    }

                    Vertex dualVertex = Algorithm.createVirtualVertex("dualVertex" + id);
                    dual.addVertex(dualVertex);
                    // TODO x y
                    id++;

                    Vertex currentVertex = vertex;
                    Edge currentEdge = edge;

                    do {
                        String pairId = currentVertex.getId() + " " + currentEdge.getId();
                        String next = pairs.get(pairId);

                        if (next == null) {
                            continue out;
                        }

                        pairs.remove(pairId);

                        Vertex[] pair = edgeToFace.get(currentEdge.getId());

                        if (pair[0] == null) {
                            pair[0] = dualVertex;
                        } else {
                            pair[1] = dualVertex;
                        }

                        currentEdge = diagram.getEdgeById(next);
                        currentVertex = outer(currentEdge, currentVertex);
                    } while (true);

                }
            }
        }

        // fill dual diagram with edges

        id = 1;

        HashMap<String, HashMap<String, Vertex>> xxx = new HashMap<String, HashMap<String, Vertex>>();

        for (Edge edge : diagram.getEdges()) {
            Vertex[] t = edgeToFace.get(edge.getId());

            Vertex dualSource = t[0];
            Vertex dualTarget = t[1];

            Edge dualEdge = Algorithm.createVirtualEdge("dualEdge" + id, dualSource, dualTarget);

            dual.addEdge(dualEdge);
            id++;

            // mappings from vertex to faces

            HashMap<String, Vertex> x = xxx.get(edge.getSource().getId());

            if (x == null) {
                x = new HashMap<String, Vertex>();
                xxx.put(edge.getSource().getId(), x);
            }

            x.put(dualSource.getId(), dualSource);
            x.put(dualTarget.getId(), dualTarget);

            x = xxx.get(edge.getTarget().getId());

            if (x == null) {
                x = new HashMap<String, Vertex>();
                xxx.put(edge.getTarget().getId(), x);
            }

            x.put(dualSource.getId(), dualSource);
            x.put(dualTarget.getId(), dualTarget);
        }

        //p(dual);

        pt("createDualDiagram(): " + (System.currentTimeMillis() - start));

        return new DualResult(dual, xxx);
    }

    private static void moveToAdjancedLayer(Diagram diagram, Vertex vertex, int distance)
    {
        if (vertex.getEdgesCount() > 2) {
            return;
        }

        int x = vertex.getX() + distance;

        Vertex horizontal = diagram.getVertexAt(x, vertex.getY());
        Vertex diagonal = diagram.getVertexAt(x, vertex.getY() + Algorithm.DEFAULT_DISTANCE);

        vertex.setX(x);

        int y;

        if (horizontal != null) {
            y = vertex.getY();
            vertex.setY(y + Algorithm.DEFAULT_DISTANCE);

            if (diagonal != null) {
                y = diagonal.getY();
                diagonal.setY(y + Algorithm.DEFAULT_DISTANCE);
            }
        }
    }

    private static void removeEmpty(Diagram diagram)
    {
        int[] limits = Algorithm.calculateLimits(diagram);

        for (int x = limits[Algorithm.RIGHT]; x >= limits[Algorithm.LEFT]; x -= Algorithm.DEFAULT_DISTANCE) {
            int count = 0;

            for (Vertex vertex : diagram.getVertices()) {
                if (vertex.getX() == x) {
                    count++;
                    break;
                }
            }

            if (count == 0) {
                for (Vertex vertex : diagram.getVertices()) {
                    if (vertex.getX() > x) {
                        vertex.setX(vertex.getX() - Algorithm.DEFAULT_DISTANCE);
                    }
                }
            }
        }

        for (int y = limits[Algorithm.DOWN]; y >= limits[Algorithm.UP]; y -= Algorithm.DEFAULT_DISTANCE) {
            int count = 0;

            for (Vertex vertex : diagram.getVertices()) {
                if (vertex.getY() == y) {
                    count++;
                    break;
                }
            }

            if (count == 0) {
                for (Vertex vertex : diagram.getVertices()) {
                    if (vertex.getY() > y) {
                        vertex.setY(vertex.getY() - Algorithm.DEFAULT_DISTANCE);
                    }
                }
            }
        }
    }

    private static int[] moveToPositive(Diagram diagram, int x, int y)
    {
        int[] limits = Algorithm.calculateLimits(diagram);

        for (Vertex vertex : diagram.getVertices()) {
            vertex.setX(vertex.getX() + (x - limits[Algorithm.LEFT]));
            vertex.setY(vertex.getY() + (y - limits[Algorithm.UP]));
        }

        for (Edge edge : diagram.getEdges()) {
            int[][] bendPoints = edge.getBendPoints();

            for (int i = 0; i < bendPoints.length; i++) {
                bendPoints[i][0] += (x - limits[Algorithm.LEFT]);
                bendPoints[i][1] += (y - limits[Algorithm.UP]);
            }

            edge.setBendPoints(bendPoints);
        }

        limits = Algorithm.calculateLimits(diagram);

        return new int[]{limits[Algorithm.UP], limits[Algorithm.RIGHT], limits[Algorithm.DOWN], limits[Algorithm.LEFT]};
    }

    private static Vertex outer(Edge edge, Vertex vertex)
    {
        return (edge.getSource() == vertex) ? edge.getTarget() : edge.getSource();
    }

    private static void p(Object printableObject)
    {
        System.out.println(printableObject);
    }

    private static void pt(Object printableObject)
    {
        System.out.println(printableObject);
    }

    private static int[] calculateLimits(Diagram diagram)
    {
        int top = Integer.MAX_VALUE;
        int right = Integer.MIN_VALUE;
        int bottom = Integer.MIN_VALUE;
        int left = Integer.MAX_VALUE;

        for (Vertex vertex : diagram.getVertices()) {
            if (vertex.getY() < top) {
                top = vertex.getY();
            }
            if (vertex.getY() > bottom) {
                bottom = vertex.getY();
            }
            if (vertex.getX() > right) {
                right = vertex.getX();
            }
            if (vertex.getX() < left) {
                left = vertex.getX();
            }
        }

        // TODO edges check

        return new int[]{top, right, bottom, left};
    }

    private static Vertex createVirtualVertex(String id)
    {
        Vertex vertex = new Vertex();
        vertex.setVirtual(true);
        vertex.setId(id);
        vertex.setWidth(100);
        vertex.setHeight(100);

        return vertex;
    }

    private static Edge createVirtualEdge(String id, Vertex source, Vertex target)
    {
        Edge edge = new Edge();
        edge.setId(id);
        edge.setSource(source);
        edge.setTarget(target);

        // dummy
        edge.setBendPoints(new int[][]{{0, 0}, {0, 0}});
        edge.setSourceAnchor(new double[]{0, 0});
        edge.setTargetAnchor(new double[]{0, 0});

        return edge;
    }

    private static void drawDirectEdge(Edge edge)
    {
        double xSource = edge.getSource().getX() + edge.getSource().getWidth() / 2;
        double ySource = edge.getSource().getY() + edge.getSource().getHeight() / 2;

        double xTarget = edge.getTarget().getX() + edge.getTarget().getWidth() / 2;
        double yTarget = edge.getTarget().getY() + edge.getTarget().getHeight() / 2;

        double xDiffSource = 0.5;
        double xDiffTarget = 0.5;

        double yDiffSource = 0.5;
        double yDiffTarget = 0.5;

        if (ySource != yTarget) {
            xDiffSource = Math.abs(0.5 * edge.getSource().getHeight() * (xSource - xTarget) / (ySource - yTarget) / edge.getSource().getWidth());
            xDiffTarget = Math.abs(0.5 * edge.getTarget().getHeight() * (xSource - xTarget) / (ySource - yTarget) / edge.getTarget().getWidth());
        }

        if (xSource != xTarget) {
            yDiffSource = Math.abs(0.5 * edge.getSource().getWidth() * (ySource - yTarget) / (xSource - xTarget) / edge.getSource().getHeight());
            yDiffTarget = Math.abs(0.5 * edge.getTarget().getWidth() * (ySource - yTarget) / (xSource - xTarget) / edge.getTarget().getHeight());
        }

        double[] sourceAnchor = new double[2];
        double[] targetAnchor = new double[2];

        if (xSource < xTarget) {
            sourceAnchor[0] = 0.5 + ((xDiffSource > 0.5) ? 0.5 : xDiffSource);
            targetAnchor[0] = 0.5 - ((xDiffTarget > 0.5) ? 0.5 : xDiffTarget);
        } else {
            sourceAnchor[0] = 0.5 - ((xDiffSource > 0.5) ? 0.5 : xDiffSource);
            targetAnchor[0] = 0.5 + ((xDiffTarget > 0.5) ? 0.5 : xDiffTarget);
        }

        if (ySource < yTarget) {
            sourceAnchor[1] = 0.5 + ((yDiffSource > 0.5) ? 0.5 : yDiffSource);
            targetAnchor[1] = 0.5 - ((yDiffTarget > 0.5) ? 0.5 : yDiffTarget);
        } else {
            sourceAnchor[1] = 0.5 - ((yDiffSource > 0.5) ? 0.5 : yDiffSource);
            targetAnchor[1] = 0.5 + ((yDiffTarget > 0.5) ? 0.5 : yDiffTarget);
        }

        edge.setSourceAnchor(sourceAnchor);
        edge.setTargetAnchor(targetAnchor);

        edge.setBendPoints(new int[][]{{0, 0}, {0, 0}});
    }

    private static void drawReflexiveEdge(Edge edge)
    {
        int x = edge.getSource().getX() + edge.getSource().getWidth();
        int y = edge.getSource().getY() + edge.getSource().getHeight();

        edge.setBendPoints(new int[][]{
            {x,      y - 20},
            {x + 20, y - 20},
            {x + 20, y + 20},
            {x - 20, y + 20},
            {x - 20, y     }
        });

        edge.setSourceAnchor(1.0, 1.0 - (20.0 / edge.getSource().getHeight()));
        edge.setTargetAnchor(1.0 - (20.0 / edge.getSource().getWidth()), 1.0);
    }

    public static void edge(Diagram diagram)
    {
        for (Edge edge : diagram.getEdges()) {
            if (edge.getSource() != edge.getTarget()) {
                Algorithm.drawDirectEdge(edge);
            } else {
                Algorithm.drawReflexiveEdge(edge);
            }
        }
    }

    public static void grid(Diagram diagram)
    {
        Collection<Vertex> vertices = diagram.getVertices();
        int size = (int) Math.ceil(Math.sqrt(vertices.size()));
        int i = 0;

        for (Vertex vertex : vertices) {
            vertex.setX((i % size + 1) * Algorithm.DEFAULT_DISTANCE);
            vertex.setY((i / size + 1) * Algorithm.DEFAULT_DISTANCE);
            i++;
        }

        for (Edge edge : diagram.getEdges()) {
            Algorithm.drawDirectEdge(edge);
        }
    }
}
