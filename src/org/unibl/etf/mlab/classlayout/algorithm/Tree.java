package org.unibl.etf.mlab.classlayout.algorithm;

import java.util.ArrayList;
import java.util.HashMap;

import org.unibl.etf.mlab.classlayout.diagram.Diagram;
import org.unibl.etf.mlab.classlayout.diagram.Edge;
import org.unibl.etf.mlab.classlayout.diagram.Vertex;

public class Tree
{
    protected Vertex base = null;
    protected Diagram tree = new Diagram();

    public int[] getTreeSize()
    {
        if (this.base == null) {
            return new int[]{0, 0};
        }

        int width = 1;
        int depth = 1;

        HashMap<String, Vertex> visited = new HashMap<String, Vertex>();
        visited.put(this.base.getId(), this.base);

        ArrayList<Vertex> currentVertices = new ArrayList<Vertex>();
        currentVertices.add(this.base);
        ArrayList<Vertex> nextVertices = new ArrayList<Vertex>();

        do {
            for (Vertex vertex : currentVertices) {
                for (Edge edge : vertex.getEdges()) {
                    Vertex outer = (visited.containsKey(edge.getSource().getId()))
                        ? edge.getTarget()
                        : edge.getSource();

                    if (!visited.containsKey(outer.getId())) {
                        visited.put(outer.getId(), outer);
                        nextVertices.add(outer);
                    }
                }
            }

            if (nextVertices.size() > 0) {
                depth++;
                width = Integer.max(width, nextVertices.size());
            } else {
                break;
            }

            currentVertices = nextVertices;
            nextVertices = new ArrayList<Vertex>();

        } while (true);

        return new int[]{width, depth};
    }

    @Override
    public String toString()
    {
        return "\nBase: " + this.base.getId() + "\n" + this.tree.toString();
    }
}
