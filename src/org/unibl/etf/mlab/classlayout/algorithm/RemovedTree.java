package org.unibl.etf.mlab.classlayout.algorithm;

import org.unibl.etf.mlab.classlayout.diagram.Edge;
import org.unibl.etf.mlab.classlayout.diagram.Vertex;

public class RemovedTree extends Tree
{
    private Edge connection;

    public Edge getConnection()
    {
        return connection;
    }

    public void setConnection(Edge connection)
    {
        this.connection = connection;
    }

    public boolean containsVertex(Vertex vertex)
    {
        return this.tree.containsVertex(vertex);
    }

    @Override
    public String toString()
    {
        String connection = (this.connection == null) ? null : this.connection.getId();
        return "\nBase: " + this.base.getId() + "\nConnection: " + connection + "\n" + this.tree.toString();
    }
}
