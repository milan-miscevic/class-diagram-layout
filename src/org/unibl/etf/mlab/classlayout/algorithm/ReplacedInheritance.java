package org.unibl.etf.mlab.classlayout.algorithm;

import java.util.ArrayList;

import org.unibl.etf.mlab.classlayout.diagram.Edge;
import org.unibl.etf.mlab.classlayout.diagram.Vertex;

public class ReplacedInheritance extends Tree
{
    private Vertex virtualVertex;
    private ArrayList<Edge> externalEdges = new ArrayList<Edge>();

    public Vertex getVirtualVertex()
    {
        return virtualVertex;
    }

    public void setVirtualVertex(Vertex virtualVertex)
    {
        this.virtualVertex = virtualVertex;
    }

    public ArrayList<Edge> getExternalEdges()
    {
        return externalEdges;
    }

    public void setExternalEdges(ArrayList<Edge> externalEdges)
    {
        this.externalEdges = externalEdges;
    }

    public void addExternalEdge(Edge edge)
    {
        this.externalEdges.add(edge);
    }
}
