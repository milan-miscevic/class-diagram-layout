package org.unibl.etf.mlab.classlayout.algorithm;

import java.util.HashMap;

import org.unibl.etf.mlab.classlayout.diagram.Vertex;

public class DijkstraResult
{
    private HashMap<String, Integer> distances;
    private HashMap<String, Vertex> previous;

    public DijkstraResult(HashMap<String, Integer> distances, HashMap<String, Vertex> previous)
    {
        super();

        this.distances = distances;
        this.previous = previous;
    }

    public HashMap<String, Integer> getDistances()
    {
        return distances;
    }

    public HashMap<String, Vertex> getPrevious()
    {
        return previous;
    }
}
