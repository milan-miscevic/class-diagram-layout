package org.unibl.etf.mlab.classlayout.algorithm;

public class AdjancedEdges
{
    private String counterclockwise;
    private String clockwise;

    public AdjancedEdges(String counterclockwise, String clockwise)
    {
        super();

        this.counterclockwise = counterclockwise;
        this.clockwise = clockwise;
    }

    public String getCounterclockwise()
    {
        return counterclockwise;
    }

    public String getClockwise()
    {
        return clockwise;
    }

    @Override
    public String toString()
    {
        return "\nCounterclockwise: " + this.counterclockwise + "\nClockwise: " + this.clockwise + "\n";
    }
}
