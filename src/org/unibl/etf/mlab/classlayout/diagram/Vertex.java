package org.unibl.etf.mlab.classlayout.diagram;

import java.util.Collection;
import java.util.HashMap;

public class Vertex
{
    private String id;
    private boolean virtual = false;

    private int x;
    private int y;
    private int width;
    private int height;

    private HashMap<String, Edge> edges = new HashMap<String, Edge>();

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public boolean isVirtual()
    {
        return virtual;
    }

    public void setVirtual(boolean virtual)
    {
        this.virtual = virtual;
    }

    public int getX()
    {
        return x;
    }

    public void setX(int x)
    {
        this.x = x;
    }

    public int getY()
    {
        return y;
    }

    public void setY(int y)
    {
        this.y = y;
    }

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }

    public int getHeight()
    {
        return height;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    public Collection<Edge> getEdges()
    {
        return edges.values();
    }

    public void setEdges(HashMap<String, Edge> edges)
    {
        this.edges = edges;
    }

    public int getEdgesCount()
    {
        return this.edges.size();
    }

    public Edge getFirstEdge()
    {
        for (Edge edge : this.edges.values()) {
            return edge;
        }

        return null;
    }

    public void addEdge(Edge edge)
    {
        this.edges.put(edge.getId(), edge);
    }

    public void removeEdge(Edge edge)
    {
        this.edges.remove(edge.getId());
    }

    public void removeAllEdges()
    {
        this.edges.clear();
    }

    @Override
    public String toString()
    {
        return "Vertex id:" + this.id + " hashCode:" + this.hashCode() + "\n" +
            "    x:" + this.x + " y:" + this.y + "\n" +
            "    width:" + this.width + " height:" + this.height + "\n" +
            "    edgeCount:" + this.edges.size();
    }
}
