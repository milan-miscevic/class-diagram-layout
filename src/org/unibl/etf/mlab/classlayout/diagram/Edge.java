package org.unibl.etf.mlab.classlayout.diagram;

public class Edge
{
    private String id;

    private boolean generalization;
    private double[] sourceAnchor;
    private double[] targetAnchor;
    private int[][] bendPoints;

    private Vertex source;
    private Vertex target;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public boolean isGeneralization()
    {
        return generalization;
    }

    public void setGeneralization(boolean generalization)
    {
        this.generalization = generalization;
    }

    public double[] getSourceAnchor()
    {
        return sourceAnchor;
    }

    public void setSourceAnchor(double[] sourceAnchor)
    {
        this.sourceAnchor = sourceAnchor;
    }

    public void setSourceAnchor(double x, double y)
    {
        this.sourceAnchor[0] = x;
        this.sourceAnchor[1] = y;
    }

    public double[] getTargetAnchor()
    {
        return targetAnchor;
    }

    public void setTargetAnchor(double[] targetAnchor)
    {
        this.targetAnchor = targetAnchor;
    }

    public void setTargetAnchor(double x, double y)
    {
        this.targetAnchor[0] = x;
        this.targetAnchor[1] = y;
    }

    public int[][] getBendPoints()
    {
        return bendPoints;
    }

    public void setBendPoints(int[][] bendPoints)
    {
        this.bendPoints = bendPoints;
    }

    public int getBendPointsCount()
    {
        return this.bendPoints.length;
    }

    public Vertex getSource()
    {
        return source;
    }

    public void setSource(Vertex source)
    {
        this.source = source;
    }

    public Vertex getTarget()
    {
        return target;
    }

    public void setTarget(Vertex target)
    {
        this.target = target;
    }

    @Override
    public String toString()
    {
        String bendPoints = "";

        for (int i = 0; i < this.bendPoints.length; i++) {
            bendPoints = bendPoints + "\n        " + this.bendPoints[i][0] + " " + this.bendPoints[i][1];
        }

        return "Edge id:" + this.id + " hashCode:" + this.hashCode() + "\n" +
            "    isGeneralization:" + this.generalization + "\n" +
            "    source:" + this.source.getId() + " target:" + this.target.getId() + bendPoints + "\n" +
            "    sourceAnchor:" + this.sourceAnchor[0] + "," + this.sourceAnchor[1] + "\n" +
            "    targetAnchor:" + this.targetAnchor[0] + "," + this.targetAnchor[1];
    }
}
