package org.unibl.etf.mlab.classlayout.diagram;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Mapper
{
    private Document document;

    public Diagram load(String filename) throws IOException, ParserConfigurationException, SAXException
    {
        File file = new File(filename);
        FileInputStream fis = new FileInputStream(file);

        return this.load(fis);
    }

    public Diagram load(InputStream inputStream) throws IOException, ParserConfigurationException, SAXException
    {
        // input stream to document

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        this.document = builder.parse(inputStream);
        this.document.getDocumentElement().normalize();

        // document to diagram

        Diagram diagram = new Diagram();
        NodeList nodes = this.document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element e = (Element) node;
                if (e.getNodeName() == "children") {
                    Vertex vertex = new Vertex();
                    vertex.setId(e.getAttribute("xmi:id"));

                    Element coordNode = this.findChildNode(e, "layoutConstraint");
                    vertex.setX(this.calculateCoord(coordNode.getAttribute("x")));
                    vertex.setY(this.calculateCoord(coordNode.getAttribute("y")));
                    vertex.setWidth(this.calculateSize(coordNode.getAttribute("width")));
                    vertex.setHeight(this.calculateSize(coordNode.getAttribute("height")));

                    diagram.addVertex(vertex);
                }

                if (e.getNodeName() == "edges") {
                    Edge edge = new Edge();
                    edge.setId(e.getAttribute("xmi:id"));

                    Element coordNode = this.findChildNode(e, "bendpoints");
                    Element sourceAnchorNode = this.findChildNode(e, "sourceAnchor");
                    Element targetAnchorNode = this.findChildNode(e, "targetAnchor");

                    edge.setGeneralization(e.getAttribute("type").equals("Generalization_Edge"));
                    edge.setSourceAnchor(
                        (sourceAnchorNode == null)
                        ? new double[] {0.0, 0.0}
                        : this.parseAnchor(sourceAnchorNode.getAttribute("id"))
                    );
                    edge.setTargetAnchor(
                        (targetAnchorNode == null)
                        ? new double[] {0.0, 0.0}
                        : this.parseAnchor(targetAnchorNode.getAttribute("id"))
                    );
                    edge.setBendPoints(this.parseBendPoints(coordNode.getAttribute("points")));

                    Vertex vertex = diagram.getVertexById(e.getAttribute("source"));

                    if (vertex == null) {
                        throw new InvalidDiagramException();
                    }

                    edge.setSource(vertex);

                    vertex = diagram.getVertexById(e.getAttribute("target"));

                    if (vertex == null) {
                        throw new InvalidDiagramException();
                    }

                    edge.setTarget(vertex);

                    diagram.addEdge(edge);
                }
            }
        }

        return diagram;
    }

    public void store(String filename, Diagram diagram) throws FileNotFoundException, TransformerException
    {
        File file = new File(filename);
        FileOutputStream fos = new FileOutputStream(file);

        this.store(fos, diagram);
    }

    public void store(OutputStream outputStream, Diagram diagram) throws TransformerException
    {
        // diagram do document

        NodeList nodes = this.document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                if (element.getNodeName() == "children") {
                    Element coordNode = this.findChildNode(element, "layoutConstraint");
                    NamedNodeMap attributes = coordNode.getAttributes();
                    Vertex vertex = diagram.getVertexById(element.getAttribute("xmi:id"));
                    if (attributes.getNamedItem("x") == null) {
                        attributes.setNamedItem(this.document.createAttribute("x"));
                    }
                    if (attributes.getNamedItem("y") == null) {
                        attributes.setNamedItem(this.document.createAttribute("y"));
                    }
                    attributes.getNamedItem("x").setTextContent(Integer.toString(vertex.getX()));
                    attributes.getNamedItem("y").setTextContent(Integer.toString(vertex.getY()));
                    // no storing of width and height
                }

                if (element.getNodeName() == "edges") {
                    Edge edge = diagram.getEdgeById(element.getAttribute("xmi:id"));
                    NamedNodeMap attributes;
                    Node attribute;

                    Element coordNode = this.findChildNode(element, "bendpoints");
                    attributes = coordNode.getAttributes();
                    attributes.getNamedItem("points").setTextContent(this.formatBendPoints(edge.getBendPoints()));

                    // sourceAnchor

                    Element sourceAnchorNode = this.findChildNode(element, "sourceAnchor");

                    if (sourceAnchorNode == null) {
                        sourceAnchorNode = this.document.createElement("sourceAnchor");
                        element.appendChild(sourceAnchorNode);
                    }

                    attributes = sourceAnchorNode.getAttributes();
                    attribute = attributes.getNamedItem("id");

                    if (attribute == null) {
                        attribute = this.document.createAttribute("id");
                        attributes.setNamedItem(attribute);
                    }

                    attribute.setTextContent(this.formatAnchor(edge.getSourceAnchor()));

                    // targetAnchor

                    Element targetAnchorNode = this.findChildNode(element, "targetAnchor");

                    if (targetAnchorNode == null) {
                        targetAnchorNode = this.document.createElement("targetAnchor");
                        element.appendChild(targetAnchorNode);
                    }

                    attributes = targetAnchorNode.getAttributes();
                    attribute = attributes.getNamedItem("id");

                    if (attribute == null) {
                        attribute = this.document.createAttribute("id");
                        attributes.setNamedItem(attribute);
                    }

                    attribute.setTextContent(this.formatAnchor(edge.getTargetAnchor()));
                }
            }
        }

        // document to output stream

        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        DOMSource source = new DOMSource(this.document);

        StreamResult result = new StreamResult(outputStream);
        transformer.transform(source, result);
    }

    private Element findChildNode(Element element, String name)
    {
        NodeList nodes = element.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element coordNode = (Element) node;
                if (coordNode.getNodeName() == name) {
                    return coordNode;
                }
            }
        }

        // or throw exception
        return null;
    }

    private int calculateCoord(String input)
    {
        if (input == "") {
            return 0;
        }

        return Integer.parseInt(input);
    }

    private int calculateSize(String input)
    {
        if (input == "") {
            return 100;
        }

        return Integer.parseInt(input);
    }

    // [500, 280, -643984, -643984]$[320, 160, -643984, -643984]
    // why 643984?

    private int[][] parseBendPoints(String input)
    {
        input = input.replace("$", ",");
        input = input.replace("[", "");
        input = input.replace("]", "");
        input = input.replace(" ", "");
        String[] tokens = input.split(",");

        int[][] output = new int[tokens.length/4][2];

        for (int i = 0; i < tokens.length; i++) {
            int index = i / 4;
            if (i % 4 == 0) {
                output[index][0] = Integer.parseInt(tokens[i]);
                output[index][1] = Integer.parseInt(tokens[i+1]);
            }
        }

        return output;
    }

    private String formatBendPoints(int[][] input)
    {
        String[] parts = new String[input.length];

        for (int i = 0; i < input.length; i++) {
            parts[i] = "[" + input[i][0] + ", " + input[i][1] + ", -643984, -643984]";
        }

        return String.join("$", parts);
    }

    // (0.2,1.0)

    private double[] parseAnchor(String input)
    {
        input = input.replace("(", "");
        input = input.replace(")", "");
        String[] tokens = input.split(",");

        double[] output = new double[2];
        output[0] = Double.parseDouble(tokens[0]);
        output[1] = Double.parseDouble(tokens[1]);

        return output;
    }

    private String formatAnchor(double[] input)
    {
        return "(" + input[0] + "," + input[1] + ")";
    }
}
