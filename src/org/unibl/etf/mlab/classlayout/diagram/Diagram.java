package org.unibl.etf.mlab.classlayout.diagram;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class Diagram
{
    private HashMap<String, Vertex> vertices = new HashMap<String, Vertex>();
    private HashMap<String, Edge> edges = new HashMap<String, Edge>();

    // vertex

    public Collection<Vertex> getVertices()
    {
        return this.vertices.values();
    }

    public Vertex getVertexById(String id)
    {
        return this.vertices.get(id);
    }

    public void addVertex(Vertex vertex)
    {
        this.vertices.put(vertex.getId(), vertex);
    }

    public void addVertices(Collection<Vertex> vertices)
    {
        for (Vertex vertex : vertices) {
            this.addVertex(vertex);
        }
    }

    public ArrayList<Edge> removeVertex(Vertex vertex)
    {
        ArrayList<Edge> toRemove = new ArrayList<>(vertex.getEdges());

        for (Edge edge : toRemove) {
            this.removeEdge(edge);
        }

        this.vertices.remove(vertex.getId());

        return toRemove;
    }

    public void removeVertices(Collection<Vertex> vertices)
    {
        for (Vertex vertex : vertices) {
            this.removeVertex(vertex);
        }
    }

    public boolean containsVertex(Vertex vertex)
    {
        return this.vertices.containsKey(vertex.getId());
    }

    public Vertex getVertexAt(int x, int y)
    {
        for (Vertex vertex : this.vertices.values()) {
            if (vertex.getX() == x && vertex.getY() == y) {
                return vertex;
            }
        }

        return null;
    }

    // edge

    public Collection<Edge> getEdges()
    {
        return this.edges.values();
    }

    public Edge getEdgeById(String id)
    {
        return this.edges.get(id);
    }

    public void addEdge(Edge edge)
    {
        this.edges.put(edge.getId(), edge);

        edge.getSource().addEdge(edge);
        edge.getTarget().addEdge(edge);
    }

    public void addEdges(Collection<Edge> edges)
    {
        for (Edge edge : edges) {
            this.addEdge(edge);
        }
    }

    public void removeEdge(Edge edge)
    {
        edge.getSource().removeEdge(edge);
        edge.getTarget().removeEdge(edge);

        this.edges.remove(edge.getId());
    }

    public void removeEdges(Collection<Edge> edges)
    {
        for (Edge edge : edges) {
            this.removeEdge(edge);
        }
    }

    public void refreshEdgeCounts()
    {
        for (Vertex vertex : this.vertices.values()) {
            vertex.removeAllEdges();
        }

        for (Edge edge : this.edges.values()) {
            edge.getSource().addEdge(edge);
            edge.getTarget().addEdge(edge);
        }
    }

    // other

    public boolean isEmpty()
    {
        return this.vertices.size() == 0;
    }

    // override

    @Override
    public String toString()
    {
        String result = "";

        for (Vertex vertex : this.getVertices()) {
            result = result + vertex.toString() + "\n";
        }

        for (Edge edge : this.getEdges()) {
            result = result + edge.toString() + "\n";
        }

        return result;
    }
}
