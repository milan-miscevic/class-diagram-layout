package org.unibl.etf.mlab.classlayout.handlers;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.unibl.etf.mlab.classlayout.algorithm.Algorithm;
import org.unibl.etf.mlab.classlayout.diagram.Diagram;
import org.unibl.etf.mlab.classlayout.diagram.Mapper;

public class ClassLayoutHandler extends AbstractHandler {

    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException
    {
        IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
        String workspacePath = ResourcesPlugin.getWorkspace().getRoot().getLocation().toString();
        IStructuredSelection selection = (IStructuredSelection) window.getSelectionService().getSelection();
        Object firstElement = selection.getFirstElement();
        IProject project;

        try {
            project = (IProject) firstElement;
        } catch (ClassCastException ex) {
            MessageDialog.openInformation(
                window.getShell(),
                "Class Layout",
                "Apply operation on project."
            );

            return null;
        }

        try {
            File folder = new File(workspacePath + project.getFullPath().toString());
            File[] listOfFiles = folder.listFiles();
            Mapper mapper = new Mapper();

            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile() && listOfFiles[i].getAbsolutePath().endsWith(".notation")) {
                    String filename = listOfFiles[i].getAbsolutePath();

                    Diagram diagram = mapper.load(filename);
                    Algorithm.grid(diagram);
                    mapper.store(filename, diagram);

                    MessageDialog.openInformation(
                        window.getShell(),
                        "Class Layout",
                        filename
                    );
                }
            }
        } catch (Exception ex) {
            MessageDialog.openInformation(
                window.getShell(),
                "Class Layout",
                ex.getMessage()
            );
        }

        return null;
    }
}
